function Segmentation()
    Init();
    
    % I = imread('img/left1.jpg');
    %I = imread('img/left53.jpg'); %% NOPE
    % I = imread('img/left381.jpg'); %% OK Method 2 / 3, OK Method 3
    %I = imread('img/left4120.jpg'); %% Ok Method with 5k/1class
    % I = imread('img/right7.jpg'); %% Method 3 with  5k/3class
    %I = imread('img/right32.jpg'); %% NOPE
    
    %Show(I,'Original');
    
   % Method1(I);
   % Method2(I);
   % Method3(I);
end


function res = Method1(img)
I = img;

I(:,:,1) = 0;
Show(I, 'No RED channel');
%%---------------------------------------
I(I <= 100) = 255;
I(I > 100 & I <= 254) = 0;
Show(I, 'Treshold set');
%%---------------------------------------
I = imbinarize(rgb2gray(I));
imshow(I);
title('Binary');

figure;
SE = strel('disk', 5);
c = imerode(I,SE);
imshow(c);
title('Binary - imerode');

res = I;
end

function res = Method2(img)
treshold = 100;

I = im2double(img);
I = rgb2lab(I);

c_picker = uisetcolor([1 0 0],'Select a color');
display(c_picker);
c_picker = rgb2lab(c_picker);

R = I(:,:,1);
G = I(:,:,2);
B = I(:,:,3);

%% Distances - from choosen color in channels
deltaR = R - c_picker(1);
deltaG = G - c_picker(2);
deltaB = B - c_picker(3);

a = sqrt(deltaG.^2 + deltaB.^2);
imagesc(a)
title('Substraction of chosen color');
figure;

treshold2 = 58;
a(a < treshold2) = 0;
imagesc(a)
title('Black treshold');
figure

b = imbinarize(a);
b = ~b;
imshow(b);
title('Binarize');

res = b;
end

function res = Method3(img)
a = K_Means(img,12);
a(a == 5) = 0;
a = ~a;
figure
imshow(a);
res = a;
end

function Show(I, t)
imshow(I);
title(t);
figure;
end

function Init()
clear all;
clc;
end