function res = MethodB(img)
I = im2double(img);
I = rgb2lab(I);

CONSTANT_RED = [47 46 62];

%% Method B with color from method R - HAND
%CONSTANT_RED = [19 4 -10];

R = I(:,:,1);
G = I(:,:,2);
B = I(:,:,3);

deltaR = R - CONSTANT_RED(1);
deltaG = G - CONSTANT_RED(2);
deltaB = B - CONSTANT_RED(3);

a = sqrt(deltaG.^2 + deltaB.^2);
imagesc(a)
title('Substraction of chosen color');
figure;

treshold2 = 79; 
a(a < treshold2) = 0;
imagesc(a)
title('Black treshold');
figure

b = imbinarize(a);
b = ~b;
b = bwareaopen(b,15);

SE = strel('disk', 3);
b = imerode(b,SE);

imshow(b);
title('Binarize');
res = b;
end