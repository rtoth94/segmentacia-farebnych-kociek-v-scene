function res = K_Means(img, K)
 I = rgb2lab(img);
 rng(1);
 
% --------- Transponse image to a column vector - N * 3 ---------
 A = reshape(I(:),[],3);

% --------- Use K-means algorithm, returns N * 1 vector ---------
 k_sorted = kmeans(A,K);

% --------- Reshape the previus vector back to image size  ---------
S = size(I);
result = reshape(k_sorted(:),[S(1), S(2)]);

imshow(label2rgb(result));

res = result;

% with_pos = DispPositions(I,K,A);
% res = with_pos;
% imshow(with_pos);
end

function res = DispPositions(img,K,A)
figure;
S = size(img);
% --------- Get the [x,y] positions of colors  ---------
[X,Y] = meshgrid(1:S(2),1:S(1));
positions = reshape([X,Y],[],2);

% --------- Add positions to A  ---------
A_modified = [A positions];

% --------- Use K-means again ---------
k_sorted2 = kmeans(A_modified,K);
result2 = reshape(k_sorted2(:),[S(1), S(2)]);

res = label2rgb(result2);  

end
