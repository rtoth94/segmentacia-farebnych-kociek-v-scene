function imageSet = load_database(mainFolder)
    if ~isdir(mainFolder)
      errorMessage = sprintf('Error: The following folder does not exist:\n%s', mainFolder);
      uiwait(warndlg(errorMessage));
      return;
    end

    filePattern = fullfile(mainFolder, '*.jpg'); % {'*.jpg';'*.bmp';'*.png','*.JPG','*.PNG'});
    files = dir(filePattern);
    imageSet = cell(length(files),1);
    for k = 1:length(files)
      baseFileName = files(k).name;
      fullFileName = fullfile(mainFolder, baseFileName);
      %fprintf(1, 'Now reading %s\n', fullFileName);
      imageSet{k} = imread(fullFileName);
    end
end