function ret = IOUmetric(I2,I3)
I2 = logical(I2);
I3 = logical(I3);

prienik = sum(sum(I2 & I3));
zjednotenie = sum(sum(I2 | I3));
ret = prienik / zjednotenie;

end